/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "common.h"


/*
*****************************************************************************************
* OPTIONS
*****************************************************************************************
*/
#define FEATURE_SPI_3WIRE   0
#define __DEBUG_PRINTF__    1

/*
*****************************************************************************************
* PINS
*****************************************************************************************
*/

// PORTC
#define PIN_PPM             2
#define PIN_SW_BIND         3
#define PIN_SW_IN1_H        4
#define PIN_SW_IN1_L        5
#define PIN_SW_IN2_H        6
#define PIN_SW_IN2_L        7
#define PIN_BUZZER          A5
#define PIN_VR_IN1          A7
#define PIN_VR_IN2          A6


#define PIN_A7105_CS        10
#define PIN_A7105_CS_PORT   PORTB
#define PIN_A7105_CS_PIN    PINB
#define PIN_A7105_CS_DDR    DDRB
#define PIN_A7105_CS_BIT    2

#define PIN_A7105_TXEN      9
#define PIN_A7105_TXEN_PORT PORTB
#define PIN_A7105_TXEN_DDR  DDRB
#define PIN_A7105_TXEN_BIT  1


#define PIN_A7105_RXEN      8
#define PIN_A7105_RXEN_PORT PORTB
#define PIN_A7105_RXEN_DDR  DDRB
#define PIN_A7105_RXEN_BIT  0


// PORTB
#define SPI_PORT            PORTB
#define SPI_PIN             PINB
#define SPI_DDR             DDRB

#define PIN_MOSI            11
#define PIN_MOSI_BIT        3

#define PIN_SCK             13
#define PIN_SCK_BIT         5

#define PIN_MISO            12
#define PIN_MISO_BIT        4


/*

Udate on 4in1 module telemetry:
- A7105: telemetry can't work since the PA is always active, TXEN is forced to 3.3V. I've verified on my hubsan no more than 3 meters...

- NRF24L01: telemetry can't work since the PA is always active, TXEN is connected to the NRF VDD_PA pin,
            which I'm not sure it's how it should be done, but since there is an additional pull-up on the TXEN pin of the PA the signal is offset to 1.8V and 2.2V when PA should be switched on (a "1" is 1.2V or more).
            I've verified on my motorcycle no more than 2 meters...

- CYRF telemetry should work since the PA TXEN pin is well driven by the Cyrf XOUT but I'm getting small range with the RX I've got. So I will put for now the fault on the RX unless I find another RX to try out.

- CC2500 telemetry should work since the PA TXEN pin is well driven by the GDO0 pin of the CC2500.
  But I've not been able to get my RX F801 working reliably with this 4in1 module.
  I've tried to change the fine frequency tunning from -127 to +127 and I'm getting time to time only a sporadic connection (by this I mean the servo move to the correct position but goes back right away to failsafe).
  I've tried my normal v2.3c module and confirm that it works fine with it.

Notes:
-Tthe CC2500 not working reliabily might come from a fault on my module. So I think someone else will have to give it a try and report.
- Above comments about TXEN and PA control have been verified using a scope so I'm sure of what I'm saying.

The RXEN pin is connected to VCC which is fine if you are driving correctly the TXEN pin since basically TXEN=0 -> RX and TXEN=1 ->TX.
The problem is the multiplexer they've added in between the RF chips and the PA/LNA.

The entry which is supposed to be connected to the A7105 is in fact connected to 3.3V.
The entry which should drive the pin TXEN to 1 or 0 from the NRF24L01 has a big pull up which completly shift the level of the control signal making it useless and force TXEN to 1.
The 2 above are the result of a bad design of the rf board.

For the CYRF and CC2500 TXEN is well driven to 1 or 0 so TX and RX should work.
But I haven't been able to use the CC2500 at all. It does not work in TX mode with the RX I have.
The Cyrf TX part works (no idea about range) but I have some doubt about the RX side.
The Cyrf and CC2500 tests just above might be due to my board being defective so I'll wait for someone to confirm.


- Pascal

https://www.rcgroups.com/forums/showthread.php?2165676-DIY-Multiprotocol-TX-Module/page70&perpage=50

*/


#endif
