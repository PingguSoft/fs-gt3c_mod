/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include "Common.h"
#include "utils.h"

class RFProtocol
{
public:
    enum {
        TX_A7105,
    };

    enum {
        PROTO_A7105_FLYSKY_AFHDS,
        PROTO_A7105_FLYSKY_AFHDS2A,
    };

    enum TXPOWER {
        TXPOWER_100uW = 0,      // -35dBm
        TXPOWER_300uW,          // -30dBm
        TXPOWER_1mW,            // -24dBm
        TXPOWER_3mW,            // -18dBm
        TXPOWER_10mW,           // -13dBm
        TXPOWER_30mW,           // - 5dBm
        TXPOWER_100mW,          //   0dBm
        TXPOWER_150mW,          // + 4dBm
        TXPOWER_LAST,
    };

    enum {
        CH_THROTTLE = 0,
        CH_RUDDER,
        CH_ELEVATOR,
        CH_AILERON,
        CH_AUX1,
        CH_AUX2,
        CH_AUX3,
        CH_AUX4,
        CH_AUX5,
        CH_AUX6,
        CH_AUX7,
        CH_AUX8,
        MAX_CHANNEL
    };

    enum {
        TRIM_RUDDER = 0,
        TRIM_ELEVATOR,
        TRIM_AILERON,
        MAX_TRIM
    };

    enum {
        INFO_STATE = 0,
        INFO_CHANNEL,
        INFO_PACKET_CTR,
        INFO_ID,
        INFO_RXID,
        INFO_RF_POWER,
    };

    // utility functions
    static u32   buildID(u8 module, u8 proto, u8 option)  { return ((u32)module << 16 | (u32)proto << 8 | option); }
    static u8    getModule(u32 id)      { return (id >> 16) & 0xff; }
    static u8    getProtocol(u32 id)    { return (id >> 8) & 0xff;  }
    static u8    getProtocolOpt(u32 id) { return id & 0xff;         }

    static void  handleTimerIntr(void);
    static void  registerCallback(RFProtocol *protocol);

    RFProtocol(u32 id);
    virtual ~RFProtocol();

    u32  getProtoID(void)           { return mProtoID; }
    u8   getModule(void)            { return (mProtoID >> 16) & 0xff; }
    u8   getProtocol(void)          { return (mProtoID >> 8) & 0xff;  }
    u8   getProtocolOpt(void)       { return mProtoID & 0xff; }
    void setControllerID(u32 id)    { mConID = id;     }
    u32  getControllerID()          { return mConID;   }

    void injectControl(u8 ch, s16 val);
    void injectControls(s16 *data, int size);
    s16  getControl(u8 ch);         // TREA order
    s16  getControlByOrder(u8 ch);  // AETR order : deviation order

    // power
    u8   getRFPower(void);
    bool isRFPowerUpdated(void);
    void clearRFPowerUpdated(void);

    void startState(u16 period);

    // for protocol
    virtual int  init(u8 bind);
    virtual int  close(void);
    virtual int  reset(void);
    virtual int  setRFPower(u8 power);
    virtual int  getInfo(u8 id, u8 *data);
    virtual int  setInfo(u8 id, u8 *data);
    virtual u16  callState(u16 now, u16 expected) = 0;
    virtual u8   isBound(void) = 0;

static u16 mNextTS;
static RFProtocol* mChild;


private:
    void initVars();

    u32  mProtoID;
    u32  mConID;
    volatile s16  mBufControls[MAX_CHANNEL];
    u8   mTXPower;
};

#endif
