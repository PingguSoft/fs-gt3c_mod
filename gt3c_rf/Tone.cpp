#include <Arduino.h>
#include "Tone.h"
#include "Utils.h"

static volatile s32 timer2_toggle_count;
static volatile u8 *timer2_pin_port;
static volatile u8 *timer2_pin_ddr;
static volatile u8 timer2_pin_mask;

static volatile u16 *Tone::mNotePtr;
static volatile u16 *Tone::mNotePtrEnd;

void _play(u16 frequency, u32 duration);

ISR(TIMER2_COMPA_vect)
{
    if (timer2_toggle_count != 0) {
        *timer2_pin_port ^= timer2_pin_mask;
        if (timer2_toggle_count > 0)
            timer2_toggle_count--;
    } else {
        TIMSK2 &= ~_BV(OCIE2A);                     // disable the interrupt
        *timer2_pin_port &= ~(timer2_pin_mask);     // keep pin low after stop
        *timer2_pin_ddr  &= ~(timer2_pin_mask);     // keep pin high-z

        if (Tone::mNotePtr != NULL) {
            if (Tone::mNotePtr < (Tone::mNotePtrEnd - 1)) {
                u16 note = pgm_read_word(Tone::mNotePtr++);
                u16 dur  = pgm_read_word(Tone::mNotePtr++);
                _play(note, dur);
            } else {
                Tone::mNotePtr = NULL;
            }
        }
    }
}

void Tone::begin(u8 tonePin)
{
    // Set the pinMode as input for hi-z by default
    pinMode(tonePin, INPUT);

    // 8 bit timer
    TCCR2A = 0;
    TCCR2B = 0;
    bitWrite(TCCR2A, WGM21, 1);
    bitWrite(TCCR2B, CS20, 1);
    timer2_pin_ddr  = portModeRegister(digitalPinToPort(tonePin));
    timer2_pin_port = portOutputRegister(digitalPinToPort(tonePin));
    timer2_pin_mask = digitalPinToBitMask(tonePin);
}

// frequency (in hertz) and duration (in milliseconds).
void _play(u16 frequency, u32 duration)
{
    u8    prescalarbits = 0b001;
    s32   toggle_count = 0;
    u32   ocr = 0;

    *timer2_pin_ddr  |= timer2_pin_mask;    // pin to output

    // if we are using an 8 bit timer, scan through prescalars to find the best fit
    ocr = F_CPU / frequency / 2 - 1;
    prescalarbits = 0b001;

    if (ocr > 255) {
        ocr = F_CPU / frequency / 2 / 8 - 1;
        prescalarbits = 0b010;

        if (ocr > 255) {
            ocr = F_CPU / frequency / 2 / 32 - 1;
            prescalarbits = 0b011;
        }
        if (ocr > 255) {
            ocr = F_CPU / frequency / 2 / 64 - 1;
            prescalarbits = 0b100;

            if (ocr > 255) {
                ocr = F_CPU / frequency / 2 / 128 - 1;
                prescalarbits = 0b101;  // ck/64
            }

            if (ocr > 255) {
                ocr = F_CPU / frequency / 2 / 256 - 1;
                prescalarbits = 0b110;
                if (ocr > 255) {
                    // can't do any better than /1024
                    ocr = F_CPU / frequency / 2 / 1024 - 1;
                    prescalarbits = 0b111;
                }
            }
        }
    }
    TCCR2B = (TCCR2B & 0b11111000) | prescalarbits;

    // Calculate the toggle count
    if (duration > 0) {
        toggle_count = 2 * frequency * duration / 1000;
    } else {
        toggle_count = -1;
    }

    // Set the OCR for the given timer,
    // set the toggle count,
    // then turn on the interrupts
    OCR2A = ocr;
    timer2_toggle_count = toggle_count;
    bitWrite(TIMSK2, OCIE2A, 1);
}

void Tone::play(u16 frequency, u32 duration)
{
    Tone::mNotePtr = NULL;
    _play(frequency, duration);
}

void Tone::stop()
{
    TIMSK2 &= ~_BV(OCIE2A);
    *timer2_pin_port &= ~(timer2_pin_mask);
}

bool Tone::isPlaying(void)
{
    return (TIMSK2 & _BV(OCIE2A));
}

void Tone::playNotes(const u16 *notes, u16 len, u8 bWait)
{
    mNotePtr    = notes;
    mNotePtrEnd = notes + len;

    u16 note = pgm_read_word(mNotePtr++);
    u16 dur  = pgm_read_word(mNotePtr++);
    _play(note, dur);
    if (bWait) {
        while(isPlaying());
    }
}
