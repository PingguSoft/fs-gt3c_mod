/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <SPI.h>
#include "RFProtocolFlyskyAFHDS.h"
#include "utils.h"

#define MAX_BIND_COUNT          2500
#define INITIAL_WAIT_uS         2400
#define PACKET_PERIOD_FLYSKY    1510
#define PACKET_PERIOD_CX20      3984

#define PROTO_OPT_WLTOYS_V9X9   0x01
#define PROTO_OPT_WLTOYS_V6X6   0x02
#define PROTO_OPT_WLTOYS_V912   0x03
#define PROTO_OPT_WLTOYS_CX20   0x04
#define PROTO_OPT_WLTOYS_MASK   0x0F

#define PROTO_OPT_FREQTUNE_MASK 0xF0
#define PROTO_OPT_FREQTUNE_IDX(val) (((val) >> 4) & 0x0F)

enum {
    // flags going to byte 10
    FLAG_V9X9_VIDEO = 0x40,
    FLAG_V9X9_CAMERA= 0x80,
    // flags going to byte 12
    FLAG_V9X9_UNK   = 0x10, // undocumented ?
    FLAG_V9X9_LED   = 0x20,
};

enum {
    // flags going to byte 13
    FLAG_V6X6_HLESS1= 0x80,
    // flags going to byte 14
    FLAG_V6X6_VIDEO = 0x01,
    FLAG_V6X6_YCAL  = 0x02,
    FLAG_V6X6_XCAL  = 0x04,
    FLAG_V6X6_RTH   = 0x08,
    FLAG_V6X6_CAMERA= 0x10,
    FLAG_V6X6_HLESS2= 0x20,
    FLAG_V6X6_LED   = 0x40,
    FLAG_V6X6_FLIP  = 0x80,
};

enum {
    // flags going to byte 14
    FLAG_V912_TOPBTN= 0x40,
    FLAG_V912_BTMBTN= 0x80,
};

static const PROGMEM u8  TBL_INIT_REGS[] = {
    0xFF, 0x42, 0x00, 0x14, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01, 0x21, 0x05, 0x00, 0x50,
    0x9e, 0x4b, 0x00, 0x02, 0x16, 0x2b, 0x12, 0x00, 0x62, 0x80, 0x80, 0x00, 0x0a, 0x32, 0xc3, 0x0f,
    0x13, 0xc3, 0x00, 0xFF, 0x00, 0x00, 0x3b, 0x00, 0x17, 0x47, 0x80, 0x03, 0x01, 0x45, 0x18, 0x00,
    0x01, 0x0f, 0xFF,
};

static const PROGMEM u8 TBL_TX_CHANS[8][4] = {
    { 0x12, 0x34, 0x56, 0x78},
    { 0x18, 0x27, 0x36, 0x45},
    { 0x41, 0x82, 0x36, 0x57},
    { 0x84, 0x13, 0x65, 0x72},
    { 0x87, 0x64, 0x15, 0x32},
    { 0x76, 0x84, 0x13, 0x52},
    { 0x71, 0x62, 0x84, 0x35},
    { 0x71, 0x86, 0x43, 0x52}
};

u8 RFProtocolFlyskyAFHDS::isBound(void)
{
    return (mBindCtr == 0);
}


s16 RFProtocolFlyskyAFHDS::getFreqOffset(void)
{
    switch (PROTO_OPT_FREQTUNE_IDX(getProtocolOpt())) {
        default:
        case 0 : return -300;
        case 1 : return 300;
        case 2 : return 65536l;
    }
    return 0;
}

int RFProtocolFlyskyAFHDS::init1(void)
{
    u8 if_calibration1;
    u8 vco_calibration0;
    u8 vco_calibration1;
    u8 reg;

    mDev.writeID(0x5475c52a);
    for (u8 i = 0; i < sizeof(TBL_INIT_REGS); i++) {
        reg = pgm_read_byte(TBL_INIT_REGS + i);
        if (getProtocolOpt() & PROTO_OPT_WLTOYS_CX20) {
            if (i == 0x0E)
                reg = 0x01;
            else if (i == 0x1F)
                reg = 0x1F;
            else if (i == 0x20)
                reg = 0x1E;
        }

        if(reg != 0xFF)
            mDev.writeReg(i, reg);
    }
    mDev.strobe(A7105_STANDBY);

    //IF Filter Bank Calibration
    mDev.writeReg(A7105_02_CALC, 1);
    //vco_current =
    mDev.readReg(A7105_02_CALC);

    u32 ms = millis();
    while(millis()  - ms < 500) {
        if(!mDev.readReg(A7105_02_CALC))
            break;
    }
    if (millis() - ms >= 500) {
        LOG(F("init fail cal1\n"));
        return 0;
    }

    if_calibration1 = mDev.readReg(A7105_22_IF_CALIB_I);
//    mDev.readReg(A7105_24_VCO_CURCAL);
    if (if_calibration1 & A7105_MASK_FBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal2\n"));
        return 0;
    }

    //VCO Current Calibration
    mDev.writeReg(A7105_24_VCO_CURCAL, 0x13); //Recomended calibration from A7105 Datasheet

    //VCO Bank Calibration
    mDev.writeReg(A7105_26_VCO_SBCAL_II, 0x3b); //Recomended limits from A7105 Datasheet

    //VCO Bank Calibrate channel 0?
    //Set Channel
    mDev.writeReg(A7105_0F_CHANNEL, 0);

    //VCO Calibration
    mDev.writeReg(A7105_02_CALC, 2);
    ms = millis();
    while(millis() - ms < 500) {
        if (!mDev.readReg(A7105_02_CALC))
            break;
    }
    if (millis() - ms >= 500) {
        LOG(F("init fail cal3\n"));
        return 0;
    }

    vco_calibration0 = mDev.readReg(A7105_25_VCO_SBCAL_I);
    if (vco_calibration0 & A7105_MASK_VBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal4 : %x\n"), vco_calibration0);
        return 0;
    }
    LOG(F("init cal4 : %x\n"), vco_calibration0);

    //Calibrate channel 0xa0?
    //Set Channel
    mDev.writeReg(A7105_0F_CHANNEL, 0xa0);
    //VCO Calibration
    mDev.writeReg(A7105_02_CALC, 2);
    ms = millis();
    while(millis() - ms < 500) {
        if(! mDev.readReg(A7105_02_CALC))
            break;
    }
    if (millis() - ms >= 500) {
        LOG(F("init fail cal5\n"));
        return 0;
    }

    vco_calibration1 = mDev.readReg(A7105_25_VCO_SBCAL_I);
    if (vco_calibration1 & A7105_MASK_VBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal6\n"));
        return 0;
    }

    //Reset VCO Band calibration
    mDev.writeReg(A7105_25_VCO_SBCAL_I, 0x08);
    mDev.setTxRxMode(TX_EN);
    mDev.setRFPower(getRFPower());
    mDev.strobe(A7105_STANDBY);

    LOG(F("init success\n"));

    return 1;
}

static const PROGMEM u8 X17_SEQ[10] = {
    0x14, 0x31, 0x40, 0x49, 0x49,    // sometime first byte is 0x15
    0x49, 0x49, 0x49, 0x49, 0x49,
};

void RFProtocolFlyskyAFHDS::applyExtFlags(void)
{
    static u8 seq;

    switch (getProtocolOpt() & PROTO_OPT_WLTOYS_MASK) {
        case PROTO_OPT_WLTOYS_V9X9:
            if (getControlByOrder(4) > 0)
                mPacketBuf[12] |= FLAG_V9X9_LED;
            if (getControlByOrder(5) > 0)
                mPacketBuf[10] |= FLAG_V9X9_VIDEO;
            if (getControlByOrder(6) > 0)
                mPacketBuf[10] |= FLAG_V9X9_CAMERA;
            if (getControlByOrder(7) > 0)
                mPacketBuf[12] |= FLAG_V9X9_UNK;
            break;

        case PROTO_OPT_WLTOYS_V6X6:
            mPacketBuf[13] = 0x03;
            mPacketBuf[14] = 0x00;
            if (getControlByOrder(4) > 0) // Lights
                mPacketBuf[14] |= FLAG_V6X6_LED;
            if (getControlByOrder(5) > 0) // flip button
                mPacketBuf[14] |= FLAG_V6X6_FLIP;
            if (getControlByOrder(6) > 0) // take picture button
                mPacketBuf[14] |= FLAG_V6X6_CAMERA;
            if (getControlByOrder(7) > 0) // video record
                mPacketBuf[14] |= FLAG_V6X6_VIDEO;
            if (getControlByOrder(8) > 0) { // headless mode
                mPacketBuf[14] |= FLAG_V6X6_HLESS1;
                mPacketBuf[13] |= FLAG_V6X6_HLESS2;
            }
            if (getControlByOrder(9) > 0) // RTH button
                mPacketBuf[14] |= FLAG_V6X6_RTH;
            if (getControlByOrder(10) > 0)
                mPacketBuf[14] |= FLAG_V6X6_XCAL;
            if (getControlByOrder(11) > 0)
                mPacketBuf[14] |= FLAG_V6X6_YCAL;

            mPacketBuf[15] = 0x10; // unknown
            mPacketBuf[16] = 0x10; // unknown
            mPacketBuf[17] = 0xAA; // unknown
            mPacketBuf[18] = 0xAA; // unknown
            mPacketBuf[19] = 0x60; // unknown, changes at irregular interval in stock TX
            mPacketBuf[20] = 0x02; // unknown
            break;

        case PROTO_OPT_WLTOYS_V912:
            seq++;
            if (seq > 9)
                seq = 0;
            mPacketBuf[12] |= 0x20; // "channel 4" bit 6 always HIGH ?
            mPacketBuf[13] = 0x00;  // unknown
            mPacketBuf[14] = 0x00;
            if (getControlByOrder(4) > 0)
                mPacketBuf[14] |= FLAG_V912_BTMBTN; // bottom button
            if (getControlByOrder(5) > 0)
                mPacketBuf[14] |= FLAG_V912_TOPBTN; // top button
            mPacketBuf[15] = 0x27; // [15] and [16] apparently hold an analog channel with a value lower than 1000
            mPacketBuf[16] = 0x03; // maybe it's there for a pitch channel for a CP copter ?
            mPacketBuf[17] = pgm_read_byte(X17_SEQ + (seq % 10)); // not sure what [17] & [18] are for, not even if they're useful
            if (seq == 0)
                mPacketBuf[18] = 0x02;
            else
                mPacketBuf[18] = 0x00;
            mPacketBuf[19] = 0x00; // unknown
            mPacketBuf[20] = 0x00; // unknown
            break;

        case PROTO_OPT_WLTOYS_CX20:
            mPacketBuf[19] = 0x00;
            mPacketBuf[20] = (mHoppingFreqIdx << 4) | 0x0A;
            break;
    }
}

void RFProtocolFlyskyAFHDS::buildPacket(u8 init)
{
    //-100% =~ 0x03e8
    //+100% =~ 0x07ca
    //Calculate:
    //Center = 0x5d9
    //1 %    = 5
    memset(mPacketBuf, 0, sizeof(mPacketBuf));
    mPacketBuf[0] = init ? 0xaa : 0x55;
    mPacketBuf[1] = (mTXID >>  0) & 0xff;
    mPacketBuf[2] = (mTXID >>  8) & 0xff;
    mPacketBuf[3] = (mTXID >> 16) & 0xff;
    mPacketBuf[4] = (mTXID >> 24) & 0xff;
    for (u8 i = 0; i < 8; i++) {
        u16 value = getControl(i);
        mPacketBuf[5 + i * 2] = value & 0xff;
        mPacketBuf[6 + i * 2] = (value >> 8) & 0xff;
    }
    applyExtFlags();
}

u16 RFProtocolFlyskyAFHDS::callState(u16 now, u16 expected)
{
    if (mBindCtr) {
        buildPacket(1);
        mCurRFChan = 1;
        mDev.writeData(mPacketBuf, MAX_PACKET_SIZE, mCurRFChan);
        mBindCtr--;
    } else {
        buildPacket(0);
        mCurRFChan = mHoppingFreqBuf[mHoppingFreqIdx & 0x0F];
        mDev.writeData(mPacketBuf, MAX_PACKET_SIZE, mCurRFChan);
        if (isRFPowerUpdated()) {
            mDev.setRFPower(getRFPower());
            clearRFPowerUpdated();
        }
        if (mFreqOffset != getFreqOffset()) {
            mFreqOffset = getFreqOffset();
            mDev.adjustLOBaseFreq(mFreqOffset);
        }
        mPacketCtr++;
    }

    mHoppingFreqIdx++;
    return PACKET_PERIOD_FLYSKY;
}

int RFProtocolFlyskyAFHDS::init(u8 bind)
{
    u8  row;
    u8  off;
    u8  temp;

    RFProtocol::registerCallback(this);
    while (1) {
        mDev.reset();
        if (init1())
            break;
    }

    mTXID = getControllerID();

    if ((mTXID & 0xf0) > 0x90)              // limit offset to 9 as higher values don't work with some RX (ie V912)
        mTXID = mTXID - 0x70;

    row = mTXID % 16;
    off = (mTXID & 0xff) / 16;

    for (u8 i = 0; i < 16; i++) {
        temp = pgm_read_byte(&TBL_TX_CHANS[row >> 1][i >> 2]);
        if (i & 0x02)
            temp &= 0x0F;
        else
            temp >>= 4;
        temp *= 0x0A;

        if (i & 0x01)
            temp+=0x50;

        if (getProtocolOpt() & PROTO_OPT_WLTOYS_CX20) {
            if (temp == 0x0A)
                temp += 0x37;
            if (temp == 0xA0) {
                if (off < 4)
                    temp = 0x37;
                else if (off < 9)
                    temp = 0x2D;
                else
                    temp = 0x29;
            }
        }
        mHoppingFreqBuf[((row & 1) ? (15 - i) : i)] = temp - off;
    }

    mPacketCtr      = 0;
    mHoppingFreqIdx = 0;
    mFreqOffset     = getFreqOffset();
    mDev.adjustLOBaseFreq(mFreqOffset);

    if (bind) {
        mBindCtr = MAX_BIND_COUNT;
    } else {
        mBindCtr = 0;
    }
    startState(INITIAL_WAIT_uS);

    return 1;
}

int RFProtocolFlyskyAFHDS::close(void)
{
    mDev.initialize();
    return (mDev.reset() ? 1L : -1L);
}

int RFProtocolFlyskyAFHDS::reset(void)
{
    return close();
}

int RFProtocolFlyskyAFHDS::getInfo(u8 id, u8 *data)
{
    u8 size;

    size = RFProtocol::getInfo(id, data);
    if (size == 0) {
        switch (id) {
            case INFO_STATE:
                *data = (mBindCtr ? 1 : 0);
                size = 1;
                break;

            case INFO_CHANNEL:
                *data = mCurRFChan;
                size = 1;
                break;

            case INFO_PACKET_CTR:
                size = sizeof(mPacketCtr);
                *((u32*)data) = mPacketCtr;
                break;
        }
    }
    return size;
}

