/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <SPI.h>

#include "config.h"
#include "utils.h"
#include "RCRcvrPPM.h"

static void calcPPM(void);


void RCRcvrPPM::init(void)
{
    for (u8 i = 0; i < sizeof(sRC) / sizeof(s16); i++)
        sRC[i] = CHAN_MIN_VALUE;

    pinMode(PIN_PPM, INPUT);
    attachInterrupt(digitalPinToInterrupt(PIN_PPM), calcPPM, FALLING);
}

void RCRcvrPPM::close(void)
{
    detachInterrupt(digitalPinToInterrupt(PIN_PPM));
}

void calcPPM(void)
{
    static u8   ch;
    static u32  lastTS;

    u32 ts = micros();
    u16 diff = ts - lastTS;

    if (diff > 2500) {
        ch = 0;
    } else {
        if (ch < MAX_CH_CNT) {
            u16 val = constrain(diff, PPM_MIN_VALUE, PPM_MAX_VALUE);
            RCRcvr::sRC[ch++] = PPM_MIN_VALUE + (PPM_MAX_VALUE - val);
        }
    }
    lastTS = ts;
}

