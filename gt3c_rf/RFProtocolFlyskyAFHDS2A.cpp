/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <EEPROM.h>
#include "RFProtocolFlyskyAFHDS2A.h"
#include "utils.h"

#define MAX_CH  8

enum{
    PACKET_STICKS = 0,
    PACKET_SETTINGS,
    PACKET_FAILSAFE,
};

enum{
    BIND1 = 0,
    BIND2,
    BIND3,
    BIND4,
    DATA1,
};

enum {
    PWM_IBUS = 0,
    PPM_IBUS,
    PWM_SBUS,
    PPM_SBUS
};

enum {
    PROTOOPTS_OUTPUTS = 0,
    PROTOOPTS_SERVO_HZ,
    PROTOOPTS_LQI_OUT,
    PROTOOPTS_FREQTUNE,
    PROTOOPTS_RXID, // todo: store that elsewhere
    PROTOOPTS_RXID2,// ^^^^^^^^^^^^^^^^^^^^^^^^^^
    LAST_PROTO_OPT,
};


#define GET_OUTPUT_IDX(val)     (val & 0x03)            // 0, 1
#define GET_SERVO_IDX(val)      ((val >> 2) & 0x03)     // 2, 3
#define GET_FREQ_TUNE_IDX(val)  ((val >> 4) & 0x03)     // 4, 5
#define GET_LQI_OUT_IDX(val)    ((val >> 6) & 0x03)     // 6, 7

/*
static const char * const afhds2a_opts[] = {
    _tr_noop("Outputs"), "PWM/IBUS", "PPM/IBUS", "PWM/SBUS", "PPM/SBUS", NULL,
    _tr_noop("Servo Hz"), "50", "400", "5", NULL,
    _tr_noop("LQI output"), "None", "Ch5", "Ch6", "Ch7", "Ch8", "Ch9", "Ch10", "Ch11", "Ch12", NULL,
    _tr_noop("Freq Tune"), "-300", "300", "655361", NULL, // big step 10, little step 1
    "RX ID", "-32768", "32767", "1", NULL, // todo: store that elsewhere
    "RX ID2","-32768", "32767", "1", NULL, // ^^^^^^^^^^^^^^^^^^^^^^^^^^
    NULL
};
*/

static const PROGMEM u8  TBL_INIT_REGS[] = {
    0xff, 0x42 | _BV(5), 0x00, 0x25, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x01, 0x3c, 0x05, 0x00, 0x50, // 00 - 0f
    0x9e, 0x4b, 0x00, 0x02, 0x16, 0x2b, 0x12, 0x4f, 0x62, 0x80, 0xff, 0xff, 0x2a, 0x32, 0xc3, 0x1f, // 10 - 1f
    0x1e, 0xff, 0x00, 0xff, 0x00, 0x00, 0x3b, 0x00, 0x17, 0x47, 0x80, 0x03, 0x01, 0x45, 0x18, 0x00, // 20 - 2f
    0x01, 0x0f // 30 - 31
};

u8 RFProtocolFlyskyAFHDS2A::isBound(void)
{
    return mState == DATA1;
}

s16 RFProtocolFlyskyAFHDS2A::getFreqOffset(void)
{
    switch (GET_FREQ_TUNE_IDX(getProtocolOpt())) {
        default:
        case 0 : return -300;
        case 1 : return 300;
        case 2 : return 65536l;
    }
    return 0;
}

u8 RFProtocolFlyskyAFHDS2A::getOutputType(void)
{
    return PPM_SBUS; //PWM_IBUS; //(GET_OUTPUT_IDX(getProtocolOpt()));
}

u16 RFProtocolFlyskyAFHDS2A::getServoHZ(void)
{
    switch (GET_SERVO_IDX(getProtocolOpt()))
    {
        case 0: return 50;
        case 1: return 400;
        case 2: return 5;
    }
    return 50;
}

u8 RFProtocolFlyskyAFHDS2A::waitCal(u8 step, u16 wait)
{
    u32     ms = millis();
    u8      over = 1;

    while(millis() - ms < wait) {
        if(!mDev.readReg(A7105_02_CALC)) {
            over = 0;
            break;
        }
    }

    if (over) {
        LOG(F("init fail cal%d\n"), step);
        return 0;
    }
    return over;
}

u8 RFProtocolFlyskyAFHDS2A::initAFHDS2A(void)
{
    u8 if_calibration1;
    u8 vco_calibration0;
    u8 vco_calibration1;
    u8 reg;

    mDev.writeID(0x5475c52a);
    for (u8 i = 0; i < 0x33; i++) {
        reg = pgm_read_byte(TBL_INIT_REGS + i);
        if(reg != 0xFF)
            mDev.writeReg(i, reg);
    }

    mDev.strobe(A7105_STANDBY);
    mDev.setTxRxMode(TX_EN);

    //IF Filter Bank Calibration
    mDev.writeReg(A7105_02_CALC, 1);
    mDev.readReg(A7105_02_CALC);
    waitCal(1, 500);
    if_calibration1 = mDev.readReg(A7105_22_IF_CALIB_I);
    if (if_calibration1 & A7105_MASK_FBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal2\n"));
        return 0;
    }

    //VCO Current Calibration
    mDev.writeReg(A7105_24_VCO_CURCAL, 0x13); //Recomended calibration from A7105 Datasheet

    //VCO Bank Calibration
    mDev.writeReg(A7105_26_VCO_SBCAL_II, 0x3b); //Recomended limits from A7105 Datasheet

    //VCO Bank Calibrate channel 0?
    //Set Channel
    mDev.writeReg(A7105_0F_CHANNEL, 0);
    mDev.writeReg(A7105_02_CALC, 2);
    waitCal(3, 500);

    vco_calibration0 = mDev.readReg(A7105_25_VCO_SBCAL_I);
    if (vco_calibration0 & A7105_MASK_VBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal4 : %x\n"), vco_calibration0);
        return 0;
    }
    LOG(F("init cal4 : %x\n"), vco_calibration0);

    //Calibrate channel 0xa0?
    //Set Channel
    mDev.writeReg(A7105_0F_CHANNEL, 0xa0);
    mDev.writeReg(A7105_02_CALC, 2);
    waitCal(5, 500);
    vco_calibration1 = mDev.readReg(A7105_25_VCO_SBCAL_I);
    if (vco_calibration1 & A7105_MASK_VBCF) {
        //Calibration failed...what do we do?
        LOG(F("init fail cal6\n"));
        return 0;
    }

    //Reset VCO Band calibration
    mDev.writeReg(A7105_25_VCO_SBCAL_I, 0x0A);
    mDev.setTxRxMode(TX_EN);
    mDev.setRFPower(getRFPower());

    mFreqOffset = getFreqOffset();
    mDev.adjustLOBaseFreq(mFreqOffset);

    mDev.strobe(A7105_STANDBY);
    mDev.setTxRxMode(TX_EN);

    LOG(F("init success\n"));

    return 1;
}


void RFProtocolFlyskyAFHDS2A::buildStickPacket(void)
{
    mPacketBuf[0] = 0x58;
    memcpy(&mPacketBuf[1], &mTXID, 4);
    memcpy(&mPacketBuf[5], &mRXID, 4);

    for(u8 ch = 0; ch < 14; ch++) {
        if (ch >= MAX_CH) {
            mPacketBuf[9  + ch * 2] = 0xdc;
            mPacketBuf[10 + ch * 2] = 0x05;
            continue;
        }
        u16 value = getControl(ch);
        mPacketBuf[9  + ch * 2] = value & 0xff;
        mPacketBuf[10 + ch * 2] = (value >> 8) & 0xff;
    }

#if 0
    // override channel with telemetry LQI
    if(Model.proto_opts[PROTOOPTS_LQI_OUT] > 0) {
        u16 val = 1000 + (Telemetry.value[TELEM_FRSKY_LQI] * 10);
        mPacketBuf[17+((Model.proto_opts[PROTOOPTS_LQI_OUT]-1)*2)] = val & 0xff;
        mPacketBuf[18+((Model.proto_opts[PROTOOPTS_LQI_OUT]-1)*2)] = (val >> 8) & 0xff;
    }
#endif
    mPacketBuf[37] = 0x00;
}


void RFProtocolFlyskyAFHDS2A::buildFailSafePacket(void)
{
    mPacketBuf[0] = 0x56;
    memcpy(&mPacketBuf[1], &mTXID, 4);
    memcpy(&mPacketBuf[5], &mRXID, 4);
    for (u8 ch = 0; ch < 14; ch++) {
        mPacketBuf[9  + ch * 2] = 0xff;
        mPacketBuf[10 + ch * 2] = 0xff;
    }
    mPacketBuf[37] = 0x00;
}

void RFProtocolFlyskyAFHDS2A::buildSettingsPacket(void)
{
    mPacketBuf[0] = 0xaa;
    memcpy(&mPacketBuf[1], &mTXID, 4);
    memcpy(&mPacketBuf[5], &mRXID, 4);
    mPacketBuf[9] = 0xfd;
    mPacketBuf[10]= 0xff;
    mPacketBuf[11]= getServoHZ() & 0xff;
    mPacketBuf[12]= (getServoHZ() >> 8) & 0xff;

    u8 output = getOutputType();
    if (output == PPM_IBUS || output == PPM_SBUS)
        mPacketBuf[13] = 0x01; // PPM output enabled
    else
        mPacketBuf[13] = 0x00;

    mPacketBuf[14]= 0x00;
    for (u8 i = 15; i < 37; i++)
        mPacketBuf[i] = 0xff;
    mPacketBuf[18] = 0x05; // ?
    mPacketBuf[19] = 0xdc; // ?
    mPacketBuf[20] = 0x05; // ?
    if (output == PWM_SBUS || output == PPM_SBUS)
        mPacketBuf[21] = 0xdd; // SBUS output enabled
    else
        mPacketBuf[21] = 0xde;
    mPacketBuf[37] = 0x00;
}

void RFProtocolFlyskyAFHDS2A::buildPacket(u8 type)
{
    switch (type) {
        case PACKET_STICKS:
            buildStickPacket();
            break;
        case PACKET_SETTINGS:
            buildSettingsPacket();
            break;
        case PACKET_FAILSAFE:
            buildFailSafePacket();
            break;
    }
}

void RFProtocolFlyskyAFHDS2A::buildBindPacket(void)
{
    u8 ch;

    memcpy(&mPacketBuf[1], &mTXID, 4);
    memset(&mPacketBuf[5], 0xff, 4);
    mPacketBuf[10] = 0x00;
    for (ch = 0; ch < NUMFREQ; ch++) {
        mPacketBuf[11 + ch] = mHoppingFreqBuf[ch];
    }
    memset(&mPacketBuf[27], 0xff, 10);
    mPacketBuf[37] = 0x00;
    switch (mState) {
        case BIND1:
            mPacketBuf[0] = 0xbb;
            mPacketBuf[9] = 0x01;
            break;
        case BIND2:
        case BIND3:
        case BIND4:
            mPacketBuf[0] = 0xbc;
            if(mState == BIND4) {
                memcpy(&mPacketBuf[5], &mRXID, 4);
                memset(&mPacketBuf[11], 0xff, 16);
            }
            mPacketBuf[9] = mState - 1;
            if(mPacketBuf[9] > 0x02)
                mPacketBuf[9] = 0x02;
            mPacketBuf[27]= 0x01;
            mPacketBuf[28]= 0x80;
            break;
    }
}

void RFProtocolFlyskyAFHDS2A::initHoppingFreqTable(u32 seed)
{
    int idx = 0;
    u32 rnd = seed;

    while (idx < NUMFREQ) {
        int i;
        int count_1_42 = 0, count_43_85 = 0, count_86_128 = 0, count_129_168=0;
        rnd = rnd * 0x0019660D + 0x3C6EF35F; // Randomization

        u8 next_ch = ((rnd >> (idx % 32)) % 0xa8) + 1;
        // Keep the distance 2 between the channels - either odd or even
        if (((next_ch ^ seed) & 0x01 ) == 0)
            continue;
        // Check that it's not duplicate and spread uniformly
        for (i = 0; i < idx; i++) {
            if(mHoppingFreqBuf[i] == next_ch)
                break;
            if(mHoppingFreqBuf[i] <= 42)
                count_1_42++;
            else if (mHoppingFreqBuf[i] <= 85)
                count_43_85++;
            else if (mHoppingFreqBuf[i] <= 128)
                count_86_128++;
            else
                count_129_168++;
        }
        if (i != idx)
            continue;

        if ((next_ch <= 42 && count_1_42 < 5)
          ||(next_ch >= 43 && next_ch <=  85 && count_43_85  < 5)
          ||(next_ch >= 86 && next_ch <= 128 && count_86_128 < 5)
          ||(next_ch >= 129 && count_129_168 < 5)) {
            mHoppingFreqBuf[idx++] = next_ch;
        }
    }
}

// Generate internal id from TX id and manufacturer id (STM32 unique id)
void RFProtocolFlyskyAFHDS2A::initTXID(void)
{
    u32 lfsr = 0x7649eca9ul;
    u8  *ptr = (u8*)&mTXID;

    // Pump zero bytes for LFSR to diverge more
    for (u8 i = 0; i < TXID_SIZE; ++i) {
        rand32_r(&lfsr, *(ptr + i));
    }

    // Pump zero bytes for LFSR to diverge more
    for (u8 i = 0; i < TXID_SIZE; ++i)
        rand32_r(&lfsr, 0);

    for (u8 i = 0; i < TXID_SIZE; ++i) {
        *(ptr + i) = lfsr & 0xff;
        rand32_r(&lfsr, i);
    }

    // Use LFSR to seed frequency hopping sequence after another
    // divergence round
    for (u8 i = 0; i < sizeof(lfsr); ++i)
        rand32_r(&lfsr, 0);

    initHoppingFreqTable(lfsr);
}


// telemetry sensors ID
enum{
    SENSOR_VOLTAGE      = 0x00,
    SENSOR_TEMPERATURE  = 0x01,
    SENSOR_RPM          = 0x02,
    SENSOR_CELL_VOLTAGE = 0x03,
    SENSOR_RX_ERR_RATE  = 0xfe,
    SENSOR_RX_RSSI      = 0xfc,
    SENSOR_RX_NOISE     = 0xfb,
    SENSOR_RX_SNR       = 0xfa,
};


void RFProtocolFlyskyAFHDS2A::updateTelemetry(u8 *packet)
{
    // AA | TXID | RXID | sensor id | sensor # | value 16 bit big endian | sensor id ......
    // max 7 sensors per packet

    u8  voltage_index = 0;
#if HAS_EXTENDED_TELEMETRY
    u8  cell_index = 0;
    u16 cell_total = 0;
#endif
    u16 value;

    for (u8 sensor = 0; sensor < 7; sensor++) {
        u8 index = 9 + (4 * sensor);

        switch (packet[index]) {
            case SENSOR_VOLTAGE:
                voltage_index++;
                if (packet[index + 1] == 0) { // Rx voltage
                    value = packet[index+3]<<8 | packet[index+2];
                    //LOG(F("V0:%d\n"), value);
                    //TELEMETRY_SetUpdated(TELEM_FRSKY_VOLT1);
                } else if(voltage_index == 2) { // external voltage sensor #1
                    value = packet[index+3]<<8 | packet[index+2];
                    //LOG(F("V1:%d\n"), value);
                    //TELEMETRY_SetUpdated(TELEM_FRSKY_VOLT2);
                }
#if HAS_EXTENDED_TELEMETRY
                else if(voltage_index == 3) // external voltage sensor #2
                {
                    value = packet[index+3]<<8 | packet[index+2];
                    //LOG(F("V2:%d\n"), value);
                    //TELEMETRY_SetUpdated(TELEM_FRSKY_VOLT3);
                }
#endif
                break;
#if HAS_EXTENDED_TELEMETRY
            case SENSOR_TEMPERATURE:
                value = ((packet[index+3]<<8 | packet[index+2]) - 400)/10;
                //LOG(F("T:%d\n"), value);
                //TELEMETRY_SetUpdated(TELEM_FRSKY_TEMP1);
                break;
            case SENSOR_CELL_VOLTAGE:
                if(cell_index < 6) {
                    value = packet[index+3]<<8 | packet[index+2];
                    //TELEMETRY_SetUpdated(TELEM_FRSKY_CELL1 + cell_index);
                    cell_total += packet[index+3]<<8 | packet[index+2];
                    //LOG(F("VC:%d\n"), cell_total);
                }
                cell_index++;
                break;
            case SENSOR_RPM:
                value = packet[index+3]<<8 | packet[index+2];
                //LOG(F("R:%d\n"), value);
                //TELEMETRY_SetUpdated(TELEM_FRSKY_RPM);
                break;
#endif
            case SENSOR_RX_ERR_RATE:
                value = 100 - packet[index + 2];
                //TELEMETRY_SetUpdated(TELEM_FRSKY_LQI);
                break;
            case SENSOR_RX_RSSI:
                value = packet[index + 2];
                //LOG(F("S:%d\n"), value);
                //TELEMETRY_SetUpdated(TELEM_FRSKY_RSSI);
                break;
            default:
                // unknown sensor ID or end of list
                break;
            }
    }
#if HAS_EXTENDED_TELEMETRY
    if(cell_index > 0) {
        value = cell_total;
        //TELEMETRY_SetUpdated(TELEM_FRSKY_ALL_CELL);
    }
#endif
}

#define WAIT_WRITE 0x80

u16 RFProtocolFlyskyAFHDS2A::callState(u16 now, u16 expected)
{
    u16 next = 3850;
    u32 start;
    u8  reg;
    u8  isData;

    if (mFreqOffset != getFreqOffset()) {
        mFreqOffset = getFreqOffset();
        mDev.adjustLOBaseFreq(mFreqOffset);
    }

    if (now - expected > 1000) {
        LOG(F("too big.. %u, %u\n"), now, expected);
    }

    switch (mState) {
        case BIND1:
        case BIND2:
        case BIND3:
            mDev.strobe(A7105_STANDBY);
            mDev.setTxRxMode(TX_EN);
            buildBindPacket();
            mDev.writeData(mPacketBuf, TXPACKET_SIZE, (mPacketCtr % 2) ? 0x0d : 0x8c);

            reg = mDev.readReg(A7105_00_MODE);
            if(!(reg & (_BV(5) | _BV(6)))) {    // FECF + CRCF OK
                mDev.strobe(A7105_RST_RDPTR);
                mDev.readData(mPacketBuf, RXPACKET_SIZE);
                if(mPacketBuf[0] == 0xbc) {
                    memcpy(&mRXID, &mPacketBuf[5], 4);
                    if(mPacketBuf[9] == 0x01) {
                        mState = BIND4;
                        mPacketCtr++;
                        return 3850;
                    }
                }
            }

            mPacketCtr++;
            mState |= WAIT_WRITE;
            next = 1700;
            break;

        case BIND1|WAIT_WRITE:
        case BIND2|WAIT_WRITE:
        case BIND3|WAIT_WRITE:
            start = micros();
            while ((micros() - start) < 700) {
                if (!(mDev.readReg(A7105_00_MODE) & _BV(0)))
                    break;
            }
            mDev.setTxRxMode(TXRX_OFF);
            mDev.strobe(A7105_RX);
            mState &= ~WAIT_WRITE;
            mState++;
            if(mState > BIND3)
                mState = BIND1;
            next = 2150;
            break;

        case BIND4:
            mDev.strobe(A7105_STANDBY);
            mDev.setTxRxMode(TX_EN);
            buildBindPacket();
            mDev.writeData(mPacketBuf, TXPACKET_SIZE, (mPacketCtr % 2) ? 0x0d : 0x8c);
            mPacketCtr++;
            mBindReply++;
            if (mBindReply >= 4) {
                mPacketCtr = 0;
                mHoppingFreqIdx = 1;
                mState = DATA1;
            }
            //mState |= WAIT_WRITE;
            next = 3850;
            break;
/*
        case BIND4|WAIT_WRITE:
            start = micros();
            while ((micros() - start) < 700) {
                if (!(mDev.readReg(A7105_00_MODE) & _BV(0)))
                    break;
            }
            mDev.setTxRxMode(RX_EN);
            mDev.strobe(A7105_RX);
            mState &= ~WAIT_WRITE;
            next = 2150;
            break;
*/
        case DATA1:
            mDev.strobe(A7105_STANDBY);
            if (mDev.readReg(A7105_00_MODE) & _BV(0))
                isData = 0;
            else
                isData = 1;

            mDev.setTxRxMode(TX_EN);
            buildPacket(mPacketType);
            mDev.writeData(mPacketBuf, TXPACKET_SIZE, mHoppingFreqBuf[mHoppingFreqIdx++]);

            if(mHoppingFreqIdx >= 16)
                mHoppingFreqIdx = 0;
            if(!(mPacketCtr % 1313))
                mPacketType = PACKET_SETTINGS;
#ifdef FAILSAFE_ENABLE
            else if(!(mPacketCtr % 1569))
                mPacketType = PACKET_FAILSAFE;
#endif
            else
                mPacketType = PACKET_STICKS; // todo : check for settings changes

            // keep transmit power in sync
            if (isRFPowerUpdated()) {
                mDev.setRFPower(getRFPower());
                clearRFPowerUpdated();
            }

            // got some data from RX ?
            // we've no way to know if RX fifo has been filled
            // as we can't poll GIO1 or GIO2 to check WTR
            // we can't check A7105_MASK_TREN either as we know
            // it's currently in transmit mode.
            reg = mDev.readReg(A7105_00_MODE);
            if(!(reg & (_BV(5) | _BV(6))) && isData) { // FECF+CRCF Ok
                mDev.strobe(A7105_RST_RDPTR);
                u8 check;
                mDev.readData(&check, 1);
                if(check == 0xaa) {
                    mDev.strobe(A7105_RST_RDPTR);
                    mDev.readData(mPacketBuf, RXPACKET_SIZE);
                    if (mPacketBuf[9] == 0xfc) { // rx is asking for settings
                        mPacketType = PACKET_SETTINGS;
                    } else {
                        updateTelemetry(mPacketBuf);
                    }
                }
            }
            mPacketCtr++;
            mState |= WAIT_WRITE;
            next = 1700;
            break;

        case DATA1|WAIT_WRITE:
            start = micros();
            while ((micros() - start) < 700) {
                if (!(mDev.readReg(A7105_00_MODE) & _BV(0)))
                    break;
            }
            mState &= ~WAIT_WRITE;
            mDev.setTxRxMode(RX_EN);
            mDev.strobe(A7105_RX);
            next = 2150;
    }

    return next;
}

int RFProtocolFlyskyAFHDS2A::init(u8 bind)
{
    u8  row;
    u8  off;
    u8  temp;

    RFProtocol::init(bind);
    RFProtocol::registerCallback(this);
    while (1) {
        mDev.reset();
        if (initAFHDS2A())
            break;
    }

    mTXID = getControllerID();
    initTXID();
    mPacketType     = PACKET_STICKS;
    mPacketCtr      = 0;
    mHoppingFreqIdx = 0;
    mBindReply      = 0;

    if (bind) {
        mState = BIND1;
    } else {
        mState = DATA1;
    }
    startState(50000);

    return 1;
}

int RFProtocolFlyskyAFHDS2A::close(void)
{
    mDev.initialize();
    return (mDev.reset() ? 1L : -1L);
}

int RFProtocolFlyskyAFHDS2A::reset(void)
{
    return close();
}

int RFProtocolFlyskyAFHDS2A::getInfo(u8 id, u8 *data)
{
    u8 size;

    size = RFProtocol::getInfo(id, data);
    if (size == 0) {
        switch (id) {
            case INFO_STATE:
                *data = (mBindCtr ? 1 : 0);
                size = 1;
                break;

            case INFO_CHANNEL:
                *data = mHoppingFreqBuf[mHoppingFreqIdx];
                size = 1;
                break;

            case INFO_PACKET_CTR:
                size = sizeof(mPacketCtr);
                *((u32*)data) = mPacketCtr;
                break;

           case INFO_RXID:
                *((u32*)data) = mRXID;
                size = sizeof(mRXID);
                break;
        }
    }
    return size;
}


int RFProtocolFlyskyAFHDS2A::setInfo(u8 id, u8 *data)
{
    u8 size = 0;

    switch (id) {
        case INFO_RXID:
            mRXID = *((u32*)data);
            size = sizeof(mRXID);
            break;
    }

    return size;
}

