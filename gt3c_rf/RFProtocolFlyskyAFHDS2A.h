/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _PROTOCOL_FLYSKY_AFHDS2A_H
#define _PROTOCOL_FLYSKY_AFHDS2A_H

#include "DeviceA7105.h"
#include "RFProtocol.h"

class RFProtocolFlyskyAFHDS2A : public RFProtocol
{
#define TXPACKET_SIZE 38
#define RXPACKET_SIZE 37
#define NUMFREQ       16
#define TXID_SIZE     4
#define RXID_SIZE     4


public:
    RFProtocolFlyskyAFHDS2A(u32 id):RFProtocol(id) { }
    ~RFProtocolFlyskyAFHDS2A() { close(); }

// for protocol
    virtual int  init(u8 bind);
    virtual int  close(void);
    virtual int  reset(void);
    virtual int  getInfo(u8 id, u8 *data);
    virtual int  setInfo(u8 id, u8 *data);
    virtual u16  callState(u16 now, u16 expected);
    virtual u8   isBound(void);

private:
    s16  getFreqOffset(void);
    u8   getOutputType(void);
    u16  getServoHZ(void);
    u8   initAFHDS2A(void);
    void buildStickPacket(void);
    void buildFailSafePacket(void);
    void buildSettingsPacket(void);
    void buildPacket(u8 type);
    void buildBindPacket(void);
    void initHoppingFreqTable(u32 seed);
    void initTXID(void);
    u8   waitCal(u8 step, u16 wait);
    void updateTelemetry(u8 *packet);

// variables
    DeviceA7105  mDev;
    u32  mTXID;
    u32  mRXID;
    u32  mPacketCtr;
    u16  mBindCtr;
    s16  mFreqOffset;
    u8   mPacketBuf[TXPACKET_SIZE];
    u8   mHoppingFreqBuf[NUMFREQ];
    u8   mHoppingFreqIdx;
    u8   mCurRFChan;
    u8   mState;
    u8   mBindReply;
    u8   mPacketType;

protected:

};

#endif
