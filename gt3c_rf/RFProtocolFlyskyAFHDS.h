/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _PROTOCOL_FLYSKY_H
#define _PROTOCOL_FLYSKY_H

#include "DeviceA7105.h"
#include "RFProtocol.h"

class RFProtocolFlyskyAFHDS : public RFProtocol
{
#define MAX_PACKET_SIZE         21

public:
    RFProtocolFlyskyAFHDS(u32 id):RFProtocol(id) { }
    ~RFProtocolFlyskyAFHDS() { close(); }

// for protocol
    virtual int  init(u8 bind);
    virtual int  close(void);
    virtual int  reset(void);
    virtual int  getInfo(u8 id, u8 *data);
    virtual int  setInfo(u8 id, u8 *data);
    virtual u16  callState(u16 now, u16 expected);
    virtual u8   isBound(void);

private:
    int  init1(void);
    void updateCRC(void);
    void buildBindPacket(u8 state);
    void buildPacket(u8 init);
    void applyExtFlags(void);
    s16  getFreqOffset(void);

// variables
    DeviceA7105  mDev;
    u32  mTXID;
    u32  mPacketCtr;
    u16  mBindCtr;
    u8   mPacketBuf[MAX_PACKET_SIZE];
    u8   mHoppingFreqBuf[16];
    u8   mHoppingFreqIdx;
    u8   mCurRFChan;
    s16  mFreqOffset;

protected:

};

#endif
