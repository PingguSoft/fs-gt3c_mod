/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <stdarg.h>
#include "common.h"
#include "utils.h"


static const u32 rand_seed = 0xb2c54a2ful;
// Linear feedback shift register with 32-bit Xilinx polinomial x^32 + x^22 + x^2 + x + 1
static const u32 LFSR_FEEDBACK = 0x80200003ul;
static const u32 LFSR_INTAP = 32-1;


static void update_lfsr(u32 *lfsr, u8 b)
{
    for (int i = 0; i < 8; ++i) {
        *lfsr = (*lfsr >> 1) ^ ((-(*lfsr & 1u) & LFSR_FEEDBACK) ^ ~((u32)(b & 1) << LFSR_INTAP));
        b >>= 1;
    }
}

u32 rand32_r(u32 *seed, u8 update)
{
    if (!seed)
        seed = (u32*)&rand_seed;
    update_lfsr(seed, update);
    return *seed;
}

u32 rand32()
{
    return rand32_r(0, 0);
}

#ifdef __DEBUG_PRINTF__
void printf(char *fmt, ... )
{
    char buf[128]; // resulting string limited to 128 chars
    va_list args;
    va_start (args, fmt );
    vsnprintf(buf, 128, fmt, args);
    va_end (args);
    Serial.print(buf);
}

void printf(const __FlashStringHelper *fmt, ... )
{
    char buf[128]; // resulting string limited to 128 chars
    va_list args;
    va_start (args, fmt);
#ifdef __AVR__
    vsnprintf_P(buf, sizeof(buf), (const char *)fmt, args); // progmem for AVR
#else
    vsnprintf(buf, sizeof(buf), (const char *)fmt, args); // for the rest of the world
#endif
    va_end(args);
    Serial.print(buf);
}
#endif

void dump(u8 *data, u16 cnt)
{
    u8  i;
    u8  b;
    u16 addr = 0;

    while (cnt) {
        printf(F("%08x - "), addr);

        for (i = 0; (i < 16) && (i < cnt); i ++) {
            b = *(data + i);
            printf(F("%02x "), b);
        }

        printf(F(" : "));
        for (i = 0; (i < 16) && (i < cnt); i ++) {
            b = *(data + i);
            if ((b > 0x1f) && (b < 0x7f))
                printf(F("%c"), b);
            else
                printf(F("."));
        }
        printf(F("\n"));
        data += i;
        addr += i;
        cnt  -= i;
    }
}

void waitSerial(const __FlashStringHelper *string)
{
    u8 avail;

    Serial.print(F("waitKey : "));
    Serial.println(string);
    while (true) {
        if (Serial.read() == '\n')
            break;
    }
    Serial.println(F("done..\n"));
}
