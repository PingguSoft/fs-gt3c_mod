/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <Arduino.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <SPI.h>
#include <EEPROM.h>
#include "Tone.h"

#include "config.h"
#include "utils.h"
#include "RFProtocolFlyskyAFHDS.h"
#include "RFProtocolFlyskyAFHDS2A.h"
#include "RCRcvrPPM.h"
#include "SPIWrapper.h"

#define FW_VERSION  0x0120
#define MAGIC_ID    0xcafebaba

enum {
    STATE_INIT = 0,
    STATE_BINDING,
    STATE_BOUND
};

static RFProtocolFlyskyAFHDS2A  mRFProto = RFProtocolFlyskyAFHDS2A(RFProtocol::buildID(RFProtocol::TX_A7105, RFProtocol::PROTO_A7105_FLYSKY_AFHDS2A, 0));
static RCRcvrPPM                mRcvr = RCRcvrPPM();

static u8   mState = STATE_INIT;
static s8   mModelIndex = -1;
static u8   mPrevSW;
static u8   mKeyPressed;
static u32  mKeyLastTS;
static u32  mPrevAin;
static Tone mTone;
static u32  mLastBeep;

#define KEY_BIND    0


u32 getRxID(u8 index)
{
    u32 rxid = 0;

    EEPROM.get(0, rxid);
    LOG(F("magic=%lx\n"), rxid);
    if (rxid == MAGIC_ID) {
        EEPROM.get(4 + (index * sizeof(u32)), rxid);
        if (rxid == 0xffffffff)
            rxid = 0;
    } else {
        rxid = 0;
    }
    LOG(F("get index=%d, rxid=%lx\n"), index, rxid);

    return rxid;
}

void setRxID(u8 index, u32 rxid)
{
    u32 magic = MAGIC_ID;

    EEPROM.put(0, magic);
    EEPROM.put(4 + (index * sizeof(u32)), rxid);
    LOG(F("set index=%d, rxid=%lx\n"), index, rxid);
}


void setup()
{
    pinMode(PIN_SW_BIND,  INPUT_PULLUP);
    pinMode(PIN_SW_IN1_H, INPUT_PULLUP);
    pinMode(PIN_SW_IN1_L, INPUT_PULLUP);
    pinMode(PIN_SW_IN2_H, INPUT_PULLUP);
    pinMode(PIN_SW_IN2_L, INPUT_PULLUP);
    pinMode(PIN_VR_IN1,   INPUT_PULLUP);
    pinMode(PIN_VR_IN2,   INPUT_PULLUP);
    pinMode(PIN_BUZZER,   OUTPUT);

    mTone.begin(PIN_BUZZER);

    mRcvr.init();
    mRFProto.setRFPower(RFProtocol::TXPOWER_10mW);
    mRFProto.setControllerID(0xcafebabe);

    Serial.begin(115200);
    LOG(F("\n\nStart!!\n"));
}

static const PROGMEM u16 kNotesBindStart[] = {
    NOTE_C5, 200,
    NOTE_E5, 200,
    NOTE_G5, 200
};

static const PROGMEM u16 kNotesInit[] = {
    NOTE_A5, 400,
};

static const PROGMEM u16 kNotesBound[] = {
    NOTE_G5, 300,
    NOTE_C5, 300,
};

static const PROGMEM u16 kNotesBinding[] = {
    NOTE_C4, 200,
};

void setState(u8 state, u32 ts)
{
    mState = state;
    switch (state) {
        case STATE_BINDING:
            mTone.playNotes(kNotesBindStart, sizeof(kNotesBindStart) / sizeof(u16), TRUE);
            break;

        case STATE_BOUND:
            mTone.playNotes(kNotesBound, sizeof(kNotesBound) / sizeof(u16), TRUE);
            break;

        case STATE_INIT:
            mTone.playNotes(kNotesBound, sizeof(kNotesInit) / sizeof(u16), TRUE);
            break;
    }
    mLastBeep = ts;
}

void loop()
{
    u8  sw1;
    u8  sw2;
    u8  index;
    u8  bind;
    u16 ain1;
    u16 ain2;
    u32 rxid;
    u32 ts = millis();

    index = (mRcvr.getRC(7) - 1000) / 50;
    if (index != mModelIndex) {
        rxid = getRxID(index);
        bind = (rxid != 0) ? 0 : 1;

        mRFProto.setInfo(RFProtocol::INFO_RXID, (u8*)&rxid);
        mRFProto.init(bind);
        mModelIndex = index;
        if (bind) {
            LOG(F("NO RXID, BINDING !!\n"));
            setState(STATE_BINDING, ts);
        }
    }

    if (digitalRead(PIN_SW_BIND) == LOW) {
        if (mKeyPressed & _BV(KEY_BIND)) {
            if (ts - mKeyLastTS > 1000) {
                switch (mState) {
                    case STATE_BINDING:
                        LOG(F("BINDING CANCELLED !!\n"));
                        mRFProto.init(0);
                        setState(STATE_INIT, ts);
                        delay(500);
                        break;

                    default:
                        mRFProto.init(1);
                        LOG(F("BINDING !!\n"));
                        setState(STATE_BINDING, ts);
                }
                while (digitalRead(PIN_SW_BIND) == LOW);
            }
        } else {
            mKeyPressed |= _BV(KEY_BIND);
            mKeyLastTS = ts;
        }
    } else if (digitalRead(PIN_SW_BIND) == HIGH && (mKeyPressed & _BV(KEY_BIND))) {
        mKeyPressed &= ~_BV(KEY_BIND);
    }

    if (mRFProto.isBound()) {
        if (mState != STATE_BOUND) {
            mRFProto.getInfo(RFProtocol::INFO_RXID, (u8*)&rxid);
            setRxID(mModelIndex, rxid);
            setState(STATE_BOUND, ts);
        }
        mRFProto.injectControls(mRcvr.getRCs(), 4);

        // 3 position toggle switches 3 = mid, 2 = high, 1 = low
        sw1 = digitalRead(PIN_SW_IN1_H) << 1 |  digitalRead(PIN_SW_IN1_L);
        mRFProto.injectControl(4, (sw1 == 0x01) ? CHAN_MIN_VALUE : ((sw1 == 0x02) ? CHAN_MAX_VALUE : CHAN_MID_VALUE));
        sw2 = digitalRead(PIN_SW_IN2_H) << 1 |  digitalRead(PIN_SW_IN2_L);
        mRFProto.injectControl(5, (sw2 == 0x01) ? CHAN_MIN_VALUE : ((sw2 == 0x02) ? CHAN_MAX_VALUE : CHAN_MID_VALUE));
        if (((sw2 << 4) | sw1) != mPrevSW) {
            LOG(F("sw1:%x, sw2:%x\n"), sw1, sw2);
            mPrevSW = (sw2 << 4) | sw1;
            //mTone.playNotes(kNotesBinding, sizeof(kNotesBinding) / sizeof(u16));
        }

        // analog VR
        ain1 = map(analogRead(PIN_VR_IN1), 0, 1023, CHAN_MIN_VALUE, CHAN_MAX_VALUE);
        mRFProto.injectControl(6, ain1);
        ain2 = CHAN_MIN_VALUE; //map(analogRead(PIN_VR_IN2), 0, 1023, CHAN_MIN_VALUE, CHAN_MAX_VALUE);
        mRFProto.injectControl(7, ain2);
        if ((u32)(((u32)ain2 << 16) | ain1) != mPrevAin) {
            LOG(F("Ain1:%4d, Ain2:%4d\n"), ain1, ain2);
            mPrevAin = (u32)(((u32)ain2 << 16) | ain1);
        }
    } else if (mState == STATE_BINDING) {
        if (ts - mLastBeep > 2000) {
            mTone.playNotes(kNotesBinding, sizeof(kNotesBinding) / sizeof(u16), TRUE);
            mLastBeep = ts;
        }
    }
}

