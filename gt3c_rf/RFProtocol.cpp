/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

// For Arduino 1.0 and earlier
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "RFProtocol.h"
#include "utils.h"

#define abs(x) (((x) > 0) ? (x) : -(x))

u16 RFProtocol::mNextTS = 0;
RFProtocol* RFProtocol::mChild = NULL;

#define us2cnt(us)  (us)

void RFProtocol::initVars(void)
{
    memset(mBufControls, 0, sizeof(mBufControls));
    mBufControls[CH_THROTTLE] = CHAN_MIN_VALUE;
    mTXPower  = TXPOWER_10mW;
}

RFProtocol::RFProtocol(u32 id)
{
    mNextTS  = 0;
    mProtoID = id;
    initVars();
}

RFProtocol::~RFProtocol()
{
    TIMSK1 &= ~BV(OCIE1A);
    close();
    LOG(F("PROTOCOL CLOSED\n"));
}

void RFProtocol::handleTimerIntr(void)
{

}

void RFProtocol::registerCallback(RFProtocol *protocol)
{
    mChild = protocol;
}

int RFProtocol::init(u8 bind)
{
    TCCR1A  = 0;                                // Timer1 normal mode 0, OCxA,B outputs disconnected
    TCCR1B  = BV(CS11);                         // Prescaler=8, => 8Mhz/8=1,000,000Hz => 1us/tick
    TCNT1   = 0;
    TIFR1   = 0xFF;
    TIMSK1  &= ~BV(OCIE1A);
    return 0;
}

int RFProtocol::close(void)
{
    return 0;
}

int RFProtocol::reset(void)
{
    return 0;
}

int RFProtocol::setRFPower(u8 power)
{
    mTXPower = (power | 0x80);
    return 0;
}

u8 RFProtocol::getRFPower(void)
{
    return (mTXPower & 0x7f);
}

bool RFProtocol::isRFPowerUpdated(void)
{
    return (mTXPower & 0x80);
}

void RFProtocol::clearRFPowerUpdated(void)
{
    mTXPower &= 0x7f;
}

int RFProtocol::getInfo(u8 id, u8 *data)
{
    u8 size = 0;

    switch (id) {
        case INFO_ID:
            size = 4;
            *((u32*)data) = (u32)getProtoID();
            break;

        case INFO_RF_POWER:
            size = 1;
            *data = mTXPower;
            break;
    }
    return size;
}

int RFProtocol::setInfo(u8 id, u8 *data)
{
    u8 size = 0;

    return size;
}

void RFProtocol::injectControl(u8 ch, s16 val)
{
    mBufControls[ch] = val;
}

void RFProtocol::injectControls(s16 *data, int size)
{
    for (int i = 0; i < size; i++)
        mBufControls[i] = *data++;

//    for (int i = size; i < MAX_CHANNEL; i++)
//        mBufControls[i] = CHAN_MIN_VALUE;
}

s16 RFProtocol::getControl(u8 ch)
{
    return mBufControls[ch];
}

void RFProtocol::startState(u16 period)
{
    mNextTS = period;

    TCNT1   = 0;
    OCR1A   = TCNT1 + us2cnt(period);
    TIFR1   = 0xFF;
    TIMSK1  = BV(OCIE1A);
}

// A E T R : deviation channel order
static const PROGMEM u8 TBL_ORDERS[4] = {
    RFProtocol::CH_AILERON,  RFProtocol::CH_ELEVATOR,
    RFProtocol::CH_THROTTLE, RFProtocol::CH_RUDDER };

s16 RFProtocol::getControlByOrder(u8 ch)
{
    if (ch < 4)
        ch = pgm_read_byte(TBL_ORDERS + ch);

    s16 val = mBufControls[ch];

    if (ch == RFProtocol::CH_AILERON || ch == RFProtocol::CH_RUDDER)
        val = -val;

    return val;
}

/*
*****************************************************************************************
* ISR for TIMER1
*****************************************************************************************
*/
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK)
{
    u16 now;
    u16 next;
    u16 ts;
    u16 diff;

    if (RFProtocol::mChild) {
        now  = TCNT1;
        next = RFProtocol::mChild->callState(now, RFProtocol::mNextTS);
        ts   = TCNT1;

        if (ts > now) {
            diff = ts - now;
        } else {
            diff = 65535 - now + ts + 1;
        }

        if (diff > next) {
            //LOG(F("elapsed time %u > %u, (%u, %u)\n"), diff, next, now, ts);
            Serial.print('!');
        }
        OCR1A = RFProtocol::mNextTS = now + us2cnt(next);
    }
}