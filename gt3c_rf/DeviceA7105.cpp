/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/
#include "DeviceA7105.h"
#include "utils.h"

#define CS_OUTPUT       PIN_A7105_CS_DDR  |= _BV(PIN_A7105_CS_BIT)
#define CS_HI           PIN_A7105_CS_PORT |= _BV(PIN_A7105_CS_BIT)
#define CS_LO           PIN_A7105_CS_PORT &= ~_BV(PIN_A7105_CS_BIT)

void DeviceA7105::initialize()
{
    CS_OUTPUT;
    CS_HI;

    PIN_A7105_TXEN_DDR |= _BV(PIN_A7105_TXEN_BIT);
    PIN_A7105_RXEN_DDR |= _BV(PIN_A7105_RXEN_BIT);
}

u8 DeviceA7105::writeReg(u8 reg, u8 data)
{
#if !(FEATURE_SPI_3WIRE)
    if (reg == A7105_0B_GPIO1_PIN1) {
        data &= 0x01;
        data |= 0x18;
    }
#endif
    CS_LO;
    mSPI.write(reg);
    mSPI.write(data);
    CS_HI;

    return 1;
}

u8 DeviceA7105::writeData(const u8 *data, u8 length, u8 channel)
{
    CS_LO;
    mSPI.write(A7105_RST_WRPTR);
    mSPI.write(A7105_05_FIFO_DATA);
    for (u8 i = 0; i < length; i++)
        mSPI.write(*data++);
    CS_HI;

    writeReg(A7105_0F_PLL_I, channel);
    strobe(A7105_TX);

    return 1;
}

u8 DeviceA7105::writeData_P(const u8 *data, u8 length,  u8 channel)
{
    CS_LO;
    mSPI.write(A7105_RST_WRPTR);
    mSPI.write(A7105_05_FIFO_DATA);
    for (u8 i = 0; i < length; i++)
        mSPI.write(pgm_read_byte(data++));
    CS_HI;

    writeReg(A7105_0F_PLL_I, channel);
    strobe(A7105_TX);

    return 1;
}

u8 DeviceA7105::readReg(u8 reg)
{
    CS_LO;
    mSPI.write(0x40 | reg);
    u8 data = mSPI.read();
    CS_HI;

    return data;
}

u8 DeviceA7105::readData(u8 *data, u8 length)
{
    strobe(A7105_RST_RDPTR);

    CS_LO;
    mSPI.write(0x40 | A7105_05_FIFO_DATA);
    for (u8 i = 0; i < length; i++)
        *data++ = mSPI.read();
    CS_HI;

    return 1;
}

u8 DeviceA7105::strobe(u8 state)
{
    CS_LO;
    mSPI.write(state);
    CS_HI;
    return 1;
}

u8 DeviceA7105::setRFPower(u8 power)
{
    /*
    Power amp is ~+16dBm so:
    TXPOWER_100uW  = -23dBm == PAC=0 TBG=0
    TXPOWER_300uW  = -20dBm == PAC=0 TBG=1
    TXPOWER_1mW    = -16dBm == PAC=0 TBG=2
    TXPOWER_3mW    = -11dBm == PAC=0 TBG=4
    TXPOWER_10mW   = -6dBm  == PAC=1 TBG=5
    TXPOWER_30mW   = 0dBm   == PAC=2 TBG=7
    TXPOWER_100mW  = 1dBm   == PAC=3 TBG=7
    TXPOWER_150mW  = 1dBm   == PAC=3 TBG=7
    */
    u8 pac, tbg;
    switch(power) {
        case 0: pac = 0; tbg = 0; break;
        case 1: pac = 0; tbg = 1; break;
        case 2: pac = 0; tbg = 2; break;
        case 3: pac = 0; tbg = 4; break;
        case 4: pac = 1; tbg = 5; break;
        case 5: pac = 2; tbg = 7; break;
        case 6: pac = 3; tbg = 7; break;
        case 7: pac = 3; tbg = 7; break;
        default: pac = 0; tbg = 0; break;
    };
    return writeReg(0x28, (pac << 3) | tbg);
}

void DeviceA7105::setTxRxMode(enum TXRX_State mode)
{
#if FEATURE_SPI_3WIRE
    if(mode == TX_EN) {
        writeReg(A7105_0B_GPIO1_PIN1, 0x33);
        writeReg(A7105_0C_GPIO2_PIN_II, 0x31);
    } else if (mode == RX_EN) {
        writeReg(A7105_0B_GPIO1_PIN1, 0x31);
        writeReg(A7105_0C_GPIO2_PIN_II, 0x33);
    } else {
        //The A7105 seems to some with a cross-wired power-amp (A7700)
        //On the XL7105-D03, TX_EN -> RXSW and RX_EN -> TXSW
        //This means that sleep mode is wired as RX_EN = 1 and TX_EN = 1
        //If there are other amps in use, we'll need to fix this
        writeReg(A7105_0B_GPIO1_PIN1, 0x33);
        writeReg(A7105_0C_GPIO2_PIN_II, 0x33);
    }
#else
    if(mode == TX_EN) {
        PIN_A7105_TXEN_PORT |=  _BV(PIN_A7105_TXEN_BIT);
        PIN_A7105_RXEN_PORT &= ~_BV(PIN_A7105_RXEN_BIT);
    } else if (mode == RX_EN) {
        PIN_A7105_TXEN_PORT &= ~_BV(PIN_A7105_TXEN_BIT);
        PIN_A7105_RXEN_PORT |=  _BV(PIN_A7105_RXEN_BIT);
    } else {
        PIN_A7105_TXEN_PORT &= ~_BV(PIN_A7105_TXEN_BIT);
        PIN_A7105_RXEN_PORT &= ~_BV(PIN_A7105_RXEN_BIT);
    }
#endif
}

u8 DeviceA7105::reset()
{
    LOG(F("RESET !!\n"));
    initialize();
    writeReg(A7105_00_MODE, 0x00);
    delayMicroseconds(1000);

    setTxRxMode(TXRX_OFF);
    u8 res = readReg(A7105_10_PLL_II);
    strobe(A7105_STANDBY);

    res = readReg(A7105_10_PLL_II);
    LOG(F("REG : %x\n"), res);

    return (res == 0x9E);
}

void DeviceA7105::writeID(u32 id)
{
    CS_LO;
    mSPI.write(A7105_06_ID_DATA);
    mSPI.write((id >> 24) & 0xFF);
    mSPI.write((id >> 16) & 0xFF);
    mSPI.write((id >> 8) & 0xFF);
    mSPI.write((id >> 0) & 0xFF);
    CS_HI;
}

void DeviceA7105::adjustLOBaseFreq(s16 offset)
{
    // LO base frequency = 32e6*(bip+(bfp/(2^16)))
    u8  bip;  // LO base frequency integer part
    u32 bfp; // LO base frequency fractional part

    if (offset < 0) {
        bip = 0x4a; // 2368 MHz
        bfp = 0xffff + ((offset * 2048) / 1000) + 1;
    } else {
        bip = 0x4b; // 2400 MHz (default)
        bfp = (offset * 2048) / 1000;
    }

    if(offset == 0)
        bfp = 0x0002; // as per datasheet, not sure why recommanded, but that's a +1kHz drift only ...

    writeReg(A7105_11_PLL_III, bip);
    writeReg(A7105_12_PLL_IV, (bfp >> 8) & 0xff);
    writeReg(A7105_13_PLL_V, bfp & 0xff);
}

