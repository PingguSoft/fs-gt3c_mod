/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef _SPI_WRAPPER_H_
#define _SPI_WRAPPER_H_

#include "config.h"
#include <Arduino.h>
#include <avr/pgmspace.h>
#include <SPI.h>
#include "config.h"


#define NOP         __asm__ __volatile__ ("nop")

#define SCK_OUTPUT  SPI_DDR  |= _BV(PIN_SCK_BIT)
#define SCK_HI      SPI_PORT |= _BV(PIN_SCK_BIT)
#define SCK_LO      SPI_PORT &= ~_BV(PIN_SCK_BIT)

#define MOSI_OUTPUT SPI_DDR |= _BV(PIN_MOSI_BIT)
#define MOSI_INPUT  SPI_DDR &= ~_BV(PIN_MOSI_BIT)

#define MOSI_HI     SPI_PORT |= _BV(PIN_MOSI_BIT)
#define MOSI_LO     SPI_PORT &= ~_BV(PIN_MOSI_BIT)
#define MOSI_IS_SET (SPI_PIN & _BV(PIN_MOSI_BIT))

class SPIWrapper
{

public:
    SPIWrapper() {
#if FEATURE_SPI_3WIRE
        MOSI_OUTPUT;
        SCK_OUTPUT;
#else
        SPI.begin();
        SPI.setBitOrder(MSBFIRST);
        SPI.setDataMode(SPI_MODE0);
        SPI.setClockDivider(SPI_CLOCK_DIV2);
#endif
    }

    ~SPIWrapper() {
#if FEATURE_SPI_3WIRE
#else
        SPI.end();
#endif
    }

    void write(u8 data) {
#if FEATURE_SPI_3WIRE
        SCK_LO;
        MOSI_LO;
        for (u8 i = 0; i < 8; i++) {
            if (data & 0x80) {
                MOSI_HI;
            } else {
                MOSI_LO;
            }
            SCK_HI;
            data <<= 1;
            SCK_LO;
        }
        MOSI_LO;
#else
        SPI.transfer(data);
#endif
    }

    u8 read(void) {
#if FEATURE_SPI_3WIRE
        u8 data = 0;

        MOSI_INPUT;
        SCK_LO;
        for (u8 i = 0; i < 8; i++) {
            data <<= 1;
            SCK_HI;
            if (MOSI_IS_SET) {
                data |= 0x01;
            }
            SCK_LO;
        }
        MOSI_OUTPUT;

        return data;
#else
        return SPI.transfer(0xFF);
#endif
    }
};

#endif
